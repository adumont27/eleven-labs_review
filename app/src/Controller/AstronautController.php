<?php

namespace App\Controller;

use phpDocumentor\Reflection\Types\Integer;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use FOS\RestBundle\Controller\Annotations as Rest;

use App\Entity\Astronaut;

class AstronautController extends AbstractController
{

    /**
     * @Rest\Get(
     *     "/astronauts",
     *     name="app_astronauts_list"
     * )
     * @Rest\View()
     */
    public function getAstronauts() : Response
    {
        $astronauts = $this->getDoctrine()
            ->getRepository(Astronaut::class)
            ->findAll();

        if (!$astronauts) {
            throw $this->createNotFoundException(
                'There\'s no astronauts !'
            );
        }

        dump( $astronauts ); die;
    }

    /**
     * @Rest\Post(
     *     "/astronaut",
     *     name = "app_astronaut_create"
     * )
     * @Rest\View( statusCode = 201 )
     * @ParamConverter("astronaut", converter="fos_rest.request_body")
     * @param Astronaut $astronaut
     * @return Response
     */
    public function createAstronaut( Astronaut $astronaut ) : Response
    {

        $em = $this->getDoctrine()->getManager();

        $em->persist( $astronaut );
        $em->flush();

        return new Response(
            'Astronaut created !',
            Response::HTTP_CREATED,
            ['content-type' => 'text/html']
        );
    }

}
